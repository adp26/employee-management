import {
  Component,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { HeaderService } from '../../service/header.service';
import { AuthService } from 'src/app/service/auth.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private headerService: HeaderService
  ) {}
  state: string = '/login';

  subscription?: Subscription;

  ngOnInit(): void {
    this.subscription = this.headerService.activatedEmitter.subscribe(
      (activePath) => {
        if (activePath === '/login') {
          this.state = activePath;
        }
        if (activePath === '/employee/list') {
          this.state = activePath;
        }
      }
    );
  }
  onLogin() {
    if (this.state == '/login') {
      this.router.navigate(['/employee/list']);
    } else {
      this.authService.logout();
    }
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }
}
