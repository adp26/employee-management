import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class HeaderService {
  constructor(private location: Location) {}
  activatedEmitter = new BehaviorSubject<string>('/login');

  setHeader() {
    this.activatedEmitter.next(this.location.path());
  }
}
