import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject, find, map, tap } from 'rxjs';
import { Employee } from '../model/employee.model';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  private jsonUrl = 'http://localhost:9000/employees';
  private employees: BehaviorSubject<Employee[]> = new BehaviorSubject<
    Employee[]
  >([]);
  private listGroup: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(
    []
  );

  constructor(private http: HttpClient) {}

  EmployeesInit(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.jsonUrl).pipe(
      tap((data) => {
        this.employees?.next(data);
        this.listGroup?.next(data.map((val) => val.group));
      })
    );
  }

  createEmployee(data: any): Observable<any> {
    data.id = data.id.toString();
    const options = { Headers, responseType: 'json' as 'blob' };
    return this.http.post<any>(this.jsonUrl, data);
  }

  setEmployee(data: Employee) {
    // const currentData = this.employees.value;
    // const updateData = [...currentData, data];
    // this.employees.next(updateData);
  }

  getEmployee(): Observable<Employee[]> {
    return this.employees.asObservable();
  }

  removeEmployee(id: any) {
    this.employees.next(
      this.employees.value.filter((value) => value.id !== id)
    );
    return this.http.delete<any>(`${this.jsonUrl}/${id}`).subscribe();
  }
  getListGroup(): Observable<string[]> {
    return this.listGroup.asObservable();
  }
  // getEmployeeId(id:number):Employee{
  //   return this.employees.subscribe(((data : Employee[])=>data.find(value=>value.id)));
  // }
}
