import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StateService {
  private searchUsernameSubject: BehaviorSubject<string> =
    new BehaviorSubject<string>('');
  private searchStatusSubject: BehaviorSubject<string> =
    new BehaviorSubject<string>('');
  private sortCriteriaSubject: BehaviorSubject<string> =
    new BehaviorSubject<string>('id');
  private currentPageSubject: BehaviorSubject<number> =
    new BehaviorSubject<number>(1);
  private itemsPerPageSubject: BehaviorSubject<number> =
    new BehaviorSubject<number>(10);

  setSearchUsername(username: string): void {
    this.searchUsernameSubject.next(username);
  }

  getSearchUsername(): Observable<string> {
    return this.searchUsernameSubject.asObservable();
  }

  setSearchStatus(status: string): void {
    this.searchStatusSubject.next(status);
  }

  getSearchStatus(): Observable<string> {
    return this.searchStatusSubject.asObservable();
  }

  setSortCriteria(criteria: string): void {
    this.sortCriteriaSubject.next(criteria);
  }

  getSortCriteria(): Observable<string> {
    return this.sortCriteriaSubject.asObservable();
  }

  setCurrentPage(page: number): void {
    this.currentPageSubject.next(page);
  }

  getCurrentPage(): Observable<number> {
    return this.currentPageSubject.asObservable();
  }

  setItemsPerPage(items: number): void {
    this.itemsPerPageSubject.next(items);
  }

  getItemsPerPage(): Observable<number> {
    return this.itemsPerPageSubject.asObservable();
  }
}
