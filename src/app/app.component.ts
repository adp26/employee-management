import { Component, OnInit } from '@angular/core';
import { AuthService } from './service/auth.service';
import { EmployeeService } from './service/employee.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private employees: EmployeeService
  ) {}

  ngOnInit() {
    this.employees.EmployeesInit().subscribe();
    this.authService.autoLogin();
  }
}
