export class User {
  constructor(
    public email: string = '',

    private _password: string = '',
    private _token: string = '',
    private _tokenExpirationDate: Date = new Date()
  ) {}

  get token() {
    if (!this._tokenExpirationDate || new Date() > this._tokenExpirationDate) {
      return null;
    }
    return this._token;
  }
}
