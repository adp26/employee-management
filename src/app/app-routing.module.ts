import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';

import { EmployeeDetailComponent } from './pages/employee-detail/employee-detail.component';
import { EmployeeListComponent } from './pages/employee-list/employee-list.component';
import { EmployeeEditComponent } from './pages/employee-edit/employee-edit.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'employee',
    canActivate: [AuthGuard],
    children: [
      { path: 'list', component: EmployeeListComponent },
      { path: 'create', component: EmployeeEditComponent },
      { path: ':id', component: EmployeeDetailComponent },
      { path: ':id/edit', component: EmployeeEditComponent },
    ],
  },
  { path: '', redirectTo: 'employee/list', pathMatch: 'full' },
  { path: '**', redirectTo: 'employee/list' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
