import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from '../../service/employee.service';

import { Buffer } from 'buffer';
import { Employee } from 'src/app/model/employee.model';
import { Location } from '@angular/common';
@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css'],
})
export class EmployeeDetailComponent implements OnInit {
  employee?: Employee | null;
  constructor(
    private employeeService: EmployeeService,
    private route: ActivatedRoute,
    private location: Location
  ) {}
  ngOnInit(): void {
    this.route.params.subscribe((val) => {
      let decodeId = Buffer.from(val['id'], 'base64')
        .toString('binary')
        .split(' ')[0];

      this.getEmployee(decodeId);
    });
  }
  getEmployee(id: any) {
    this.employeeService.getEmployee().subscribe((employees) => {
      this.employee = employees.find((employee) => employee.id === id);
    });
  }

  goBack(): void {
    this.location.back();
  }
}
