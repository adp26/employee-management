import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Location } from '@angular/common';

import { EmployeeService } from 'src/app/service/employee.service';
import { Employee } from 'src/app/model/employee.model';

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css'],
})
export class EmployeeEditComponent implements OnInit {
  @ViewChild('employeeForm', { static: false }) employeeForm?: NgForm;
  today: string = new Date().toISOString().split('T')[0];

  isError: boolean = false;
  groups: string[] = [];
  filteredGroups: string[] = [];
  maxDescriptionLength: number = 500;
  remainingCharacters: number = this.maxDescriptionLength;
  birthDateInvalid: boolean = false;
  employeeData: Employee = {
    id: 0,
    username: '',
    firstName: '',
    lastName: '',
    email: '',
    birthDate: new Date(),
    basicSalary: '',
    status: '',
    group: '',
    description: '',
  };
  constructor(
    private employeeService: EmployeeService,
    private location: Location
  ) {}
  submitForm(): void {
    if (this.employeeForm?.invalid) {
      return;
    }
    this.employeeService.getEmployee().subscribe((value) => {
      this.employeeData.id = value.length + 1;
      this.employeeData.basicSalary = this.employeeData.basicSalary.toString();

      this.employeeService.createEmployee(this.employeeData).subscribe(
        () => {
          this.loadEmployees();
          this.onCancel();
        },
        () => {
          this.isError = true;
          setTimeout(() => {
            this.isError = false;
          }, 3000);
        }
      );
    });
  }
  loadEmployees(): void {
    this.employeeService.EmployeesInit().subscribe();
  }

  validateDate(): void {
    const birthDate = new Date(this.employeeData.birthDate);
    const todayDate = new Date(this.today);
    this.birthDateInvalid = birthDate > todayDate;
  }

  displayGroup(group: string): string {
    return group ? group : '';
  }

  filterGroups(event: Event): void {
    const value = (event.target as HTMLInputElement).value.toLowerCase();
    this.filteredGroups = this.groups.filter((group) =>
      group.toLowerCase().includes(value)
    );
  }

  updateCharacterCount(): void {
    this.remainingCharacters =
      this.maxDescriptionLength - this.employeeData.description.length;
  }
  ngOnInit(): void {
    this.employeeService.getListGroup().subscribe((data) => {
      this.groups = [...new Set(data)];
      this.filteredGroups = this.groups.slice();
    });
  }

  onCancel(): void {
    this.employeeForm?.reset();
    this.location.back();
  }
}
