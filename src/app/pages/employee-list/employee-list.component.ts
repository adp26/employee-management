import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  OnInit,
} from '@angular/core';
import { Employee } from 'src/app/model/employee.model';
import { EmployeeService } from '../../service/employee.service';
import { HeaderService } from 'src/app/service/header.service';

import { Router } from '@angular/router';

import { Buffer } from 'buffer';
import { StateService } from 'src/app/service/state.service';
import { MatDialog } from '@angular/material/dialog';
import { PopupComponent } from 'src/app/components/popup/popup.component';

interface Toast {
  message: string;
  type: 'edit' | 'delete' | 'search';
}

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css'],
})
export class EmployeeListComponent implements OnInit {
  employees: Employee[] = [];
  filteredEmployees: Employee[] = [];

  itemsPerPage: number = 10;
  itemsPerPageOptions: number[] = [];
  currentPage: number = 1;

  searchUsername: string = '';
  searchStatus: string = '';

  sortCriteria: string = 'id';
  currentSortLabel: string = 'ID';

  toasts: Toast[] = [];

  constructor(
    private router: Router,
    private employeeService: EmployeeService,
    private headerService: HeaderService,
    private stateService: StateService,
    public dialog: MatDialog
  ) {
    this.stateService
      .getSearchUsername()
      .subscribe((username) => (this.searchUsername = username));
    this.stateService
      .getSearchStatus()
      .subscribe((status) => (this.searchStatus = status));
    this.stateService.getSortCriteria().subscribe((criteria) => {
      this.sortCriteria = criteria;
      this.currentSortLabel = criteria === 'fullName' ? 'Full Name' : criteria;
    });
    this.stateService
      .getCurrentPage()
      .subscribe((page) => (this.currentPage = page));
    this.stateService
      .getItemsPerPage()
      .subscribe((items) => (this.itemsPerPage = items));
  }

  ngOnInit(): void {
    this.headerService.setHeader();
    this.employeeService.getEmployee().subscribe((data: Employee[]) => {
      this.employees = data;

      this.filteredEmployees = data;
      this.updateShowItems();
      this.applyFiltersAndSort();
    });
  }

  applyFiltersAndSort(): void {
    this.filteredEmployees = this.employees
      .filter(
        (employee) =>
          employee.username
            .toLowerCase()
            .includes(this.searchUsername.toLowerCase()) &&
          employee.status
            .toLowerCase()
            .includes(this.searchStatus.toLowerCase())
      )
      .sort((a, b) => {
        if (this.sortCriteria === 'id') {
          return a.id - b.id;
        } else if (this.sortCriteria === 'fullName') {
          const fullNameA = `${a.firstName.toLowerCase()} ${a.lastName.toLowerCase()}`;
          const fullNameB = `${b.firstName.toLowerCase()} ${b.lastName.toLowerCase()}`;

          return fullNameA < fullNameB ? -1 : fullNameA > fullNameB ? 1 : 0;
        } else {
          let fieldA: string = (
            a[this.sortCriteria as keyof Employee] as string
          ).toLowerCase();
          let fieldB: string = (
            b[this.sortCriteria as keyof Employee] as string
          ).toLowerCase();
          return fieldA < fieldB ? -1 : fieldA > fieldB ? 1 : 0;
        }
      });

    this.updateShowItems();
  }

  searchEmployees(): void {
    this.currentPage = 1;
    this.applyFiltersAndSort();
    if (this.filteredEmployees.length === 0) {
      this.addToast(
        'No employees found with the given search criteria',
        'search'
      );
    }
  }

  sortEmployees(criteria: string, label: string): void {
    this.currentSortLabel = label;
    this.sortCriteria = criteria;

    this.applyFiltersAndSort();
    this.updateShowItems();
  }

  updateShowItems(): void {
    this.itemsPerPageOptions = this.generateItemsPerPageOptions(
      this.filteredEmployees.length
    );
  }

  resetSearchUsername(): void {
    //di eksekusi setiap perubahan input  username
    if (this.searchUsername === '') {
      this.applyFiltersAndSort();
    }
  }
  resetSearchStatus(): void {
    //di eksekusi setiap perubahan input  username
    if (this.searchStatus === '') {
      this.applyFiltersAndSort();
    }
  }

  generateItemsPerPageOptions(totalItems: number): number[] {
    const options: number[] = [];
    for (let i = 10; i <= totalItems; i += 10) {
      options.push(i);
    }
    return options;
  }

  changeItemsPerPage(items: number): void {
    this.itemsPerPage = items;
    this.currentPage = 1;
    this.updateShowItems();
  }

  createEmployee() {
    this.saveState();
    this.router.navigate(['/employee/create']);
  }

  detailEmployee(employee: Employee): void {
    this.saveState();
    let encodeId = Buffer.from(`${employee.id} as234`).toString('base64');
    this.router.navigate(['/employee', encodeId]);
  }

  saveState() {
    this.stateService.setSearchUsername(this.searchUsername);
    this.stateService.setSearchStatus(this.searchStatus);
    this.stateService.setSortCriteria(this.sortCriteria);
    this.stateService.setCurrentPage(this.currentPage);
    this.stateService.setItemsPerPage(this.itemsPerPage);
  }

  editEmployee(employee: Employee): void {
    this.addToast(`Employee ${employee.username} is being edited`, 'edit');
  }

  deleteEmployee(employee: Employee): void {
    const dialogRef = this.dialog.open(PopupComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        // this.employeeService.removeEmployee(employee.id);
        console.log('Item deleted');
        this.addToast(
          `Employee ${employee.username} has been deleted`,
          'delete'
        );
      } else {
        // Cancel
        console.log('Delete action cancelled');
      }
    });
  }

  addToast(message: string, type: 'edit' | 'delete' | 'search') {
    this.toasts.push({ message, type });
    setTimeout(() => {
      this.toasts.shift();
    }, 3000);
  }

  removeToast(toast: Toast) {
    this.toasts = this.toasts.filter((t) => t !== toast);
  }
}
