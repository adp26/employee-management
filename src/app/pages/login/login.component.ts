import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/service/auth.service';
import { HeaderService } from 'src/app/service/header.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private router: Router,
    private headerService: HeaderService,
    private auth: AuthService
  ) {}
  signinForm!: FormGroup;
  passwordFieldType: string = 'password';
  eyeIconClass: string = 'fa fa-eye';
  ngOnInit(): void {
    this.headerService.setHeader();
    this.signinForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.maxLength(30),
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.maxLength(40),
        Validators.minLength(8),
        Validators.pattern(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)(?=^\S*$)/), // number, huruf besar dan kecil, spesial karakter, gabisa ada spasi
      ]),
    });
    this.signinForm.setValue({
      email: 'test@gmail.com',
      password: '1tesT1234@',
    });
  }

  togglePasswordVisibility(): void {
    this.passwordFieldType =
      this.passwordFieldType === 'password' ? 'text' : 'password';
  }

  onSubmit() {
    const result = this.auth.login(
      this.signinForm.get('email')?.value,
      this.signinForm.get('password')?.value
    );

    if (result) {
      this.signinForm.reset();
      this.router.navigate(['employee/list']);
    } else {
      console.log('please try again');
    }
  }
}
