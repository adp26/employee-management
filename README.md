# EmployeeManagement

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.3.

## How to run this app

- Navigate the terminal to the project directory
- run "npm install" in terminal
- add another terminal and run "npm run server"
- after finish at all, run ng serve

  Note:
  if you still encounter errors when running the application, check the node version and angular version,
  here I am using angular 15 and node 18.13.0.
  If there are still errors when running, maybe there is a third party that needs to be installed, such as Angular Material,Angular Animations, Bootstrap, Json-Server, ngx-pagination, PoperJS/Core.
  please install via terminal.

  Following are the installation commands for each third party
  -poperjs/core
  "npm install @popperjs/core"
  -ngx-pagination
  "npm i ngx-pagination"
  -json-server
  "npm i json-server"
  -angular material
  "npm i @angular/material"
  -angular animation
  "npm i @angular/animations"
  -bootstrap
  "npm i bootstrap"
